'use strict';

module.exports = {
    supportedFfmpegParameters: [
        { option: '-y', isSucceededByValue: false, configProperty: 'override' },
        { option: '-r', isSucceededByValue: true, configProperty: 'fps'},
        { option: '-f', isSucceededByValue: true, configProperty: 'encode', defaultValue: 'platformDependant' },
        { option: '-i', isSucceededByValue: true, configProperty: 'input', defaultValue: 'platformDependant' },
        { option: '-vcodec', isSucceededByValue: true, configProperty: 'videoCodec' },
        { option: '-pix_fmt', isSucceededByValue: true, configProperty: 'pixelFormat' }
    ],
    systemDependantParameters: {
        windows: {
            input: 'desktop',
            encode: 'gdigrab'
        },
        mac: {
            input: '1',
            encode: 'avfoundation'
        },
        linux: {
            input: '',
            encode: 'x11grab'
        }
    }
};
