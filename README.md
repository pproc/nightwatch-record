# Nightwatch.js video screen recording via ffmpeg

## Introduction
Repository was initially created by Ignacio Marenco and [was located here](https://github.com/imarenco/nightwatch-record).

I cloned this repository to NetEnt bitbucket, because it needed some customization.

Repository records videos of [Nightwatch.js](http://nightwatchjs.org/) test sessions, support multiple Operative Systems like MacOs, Windows, Linux.

Uses [ffmpeg](https://www.ffmpeg.org/) to capture a (remote) webdriver desktop screen.

Repository created during my full-time job in NetEnt company in Krakow, Poland.

## Install

```sh
  npm install --save-dev nightwatch-record
```

## Usage

Add the following `Before`/`After` hooks:
```js
const { client } = require("nightwatch-cucumber");
const { defineSupportCode } = require("cucumber");
 
defineSupportCode(({ Before, After }) => {
    Before("@recordVideoLocally", (browser, done) => {
        require("@netent/nightwatch-record").start(client, browser, done);
    });

    After("@recordVideoLocally", (browser, done) => {
        require("@netent/nightwatch-record").stop(client, browser, done);
    });
});
```

Enable the video screen recording in your test settings:
```json
{
  "test_settings": {
    "default": {
      "video": {
        "enabled": true,
        "delete_on_success": false,
        "path": "files/videos/",
        "fileFormat": "mp4",
        "ffmpegParameters": {
          "fps": 15,
          "videoCodec": "libx264",
          "pixelFormat": "yuv420p",
          "override": true,
          "additionalParameters": "-ar 20000"
        }
      }
    }
  }
}
```

## Author
[Piotr Proc](piotr.proc@gmail.com)