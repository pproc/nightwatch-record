'use strict';

const config = require('../config');

const getPlatform = () => {
    const operatingSystems = [
        { name: 'windows', regex: /^win/ },
        { name: 'mac', regex: /^darwin/ },
        { name: 'linux', regex: /^linux/ }
    ];

    for (const system of operatingSystems) {
        if (system.regex.test(process.platform)) {
            return config.systemDependantParameters[system.name];
        }
    }

    return null;
};

module.exports = { getPlatform };
