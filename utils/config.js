'use strict';

const parametersSupported = require('../config').supportedFfmpegParameters;
const { getPlatform } = require('./process');

const configUtil = {
    generateFfmpegArrayParametrs: (config, path) => {
        let arrayParameters = [];

        for (const optionConfig of parametersSupported) {
            configUtil._addFfmpegOption(config, arrayParameters, optionConfig);
        }

        if (config.additionalParameters) {
            arrayParameters = arrayParameters.concat(config.additionalParameters.split(' '));
        }

        arrayParameters.push(path);
        return arrayParameters;
    },

    _addFfmpegOption(config, array, optionConfig) {
        const property = optionConfig.configProperty;

        if (configUtil._isPropertyHandled(config, property)) {
            const value = config[property];

            if (optionConfig.isSucceededByValue) {
                array.push(optionConfig.option, value);
            }
            else if (value) {
                array.push(optionConfig.option);
            }
        }
        else if (optionConfig.defaultValue) {
            array.push(optionConfig.option, getPlatform()[property]);
        }
    },

    _isPropertyHandled: (config, property) => {
        return config.hasOwnProperty(property);
    }
};

module.exports = configUtil;
