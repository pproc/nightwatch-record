'use strict';

const path = require('path');

const fileUtil = {
    createDirectory: (directory, mode) => {
        const path = require('path');
        const fs = require('fs');

        directory = path.resolve(directory);
        if (fs.existsSync(directory)) return directory;
        try {
            fs.mkdirSync(directory, mode);
            return directory;
        } catch (error) {
            if (error.code === 'ENOENT') {
                return fileUtil.createDirectory(path.dirname(directory), mode) && fileUtil.createDirectory(directory, mode);
            }
            throw error;
        }
    },

    resolvePath: (filePath, testScenarioName, fileFormat) => {
        const fileName = fileUtil._getFileName(testScenarioName, fileFormat);

        return path.resolve(path.join(filePath, fileName));
    },

    _getFileName: (testScenarioName, fileFormat) => {
        const testScenarioModifiedName = testScenarioName
            .substr(0, 10)
            .toLowerCase()
            .replace(/\s/g, '');

        const date = new Date();
        const datetime =
            `${date.getDate()}`
            + `-${date.getMonth() + 1}`
            + `-${date.getFullYear()}`
            + `@${date.getHours()}`
            + `-${date.getMinutes()}`
            + `-${date.getSeconds()}`
            + `-${date.getMilliseconds()}`;

        return `${testScenarioModifiedName}-${datetime}.${fileFormat}`;
    }
};


module.exports = fileUtil;
