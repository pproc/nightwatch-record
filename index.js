'use strict';

const path = require('path');
const configUtil = require('./utils/config');
const fileUtil = require('./utils/file');

let videoSettings;
let file;

module.exports = {
    start: function(client, browser, done) {
        videoSettings = client.globals.test_settings.video;

        if (videoSettings && videoSettings.enabled) {
            const fileFormat = videoSettings.fileFormat || 'mp4';

            file = fileUtil.resolvePath(videoSettings.path, browser.pickle.name, fileFormat);
            fileUtil.createDirectory(path.dirname(file));

            const parameters = configUtil.generateFfmpegArrayParametrs(videoSettings.ffmpegParameters, file);

            client.globals.ffmpeg = require('child_process').execFile('ffmpeg',
                parameters,
                function(error) {
                    client.globals.ffmpeg = null;
                    if (error) {
                        throw error;
                    }
                }
            );
        }

        done();
    },
    stop: function(client, browser, done) {
        const testResults = browser.result.status;
        const didTestPass = testResults === 'passed';

        if (client.globals.ffmpeg) {
            if (videoSettings.deleteOnSuccess && didTestPass) {
                require('fs').unlink(file, function(err) {
                    if (err) throw err;
                });
            }

            client.globals.ffmpeg.stdin.setEncoding('utf8');
            client.globals.ffmpeg.stdin.write('q');
            done();
        }
        else {
            done();
        }
    }
};
